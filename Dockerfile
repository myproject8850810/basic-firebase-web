FROM node:16

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install firebase && npm install -g firebase-tools

COPY . .

EXPOSE 8080
CMD [ "node", "server.js" ]